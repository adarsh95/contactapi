var express = require('express');
var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'testapi',
    });


    module.exports = {
/* insert into table if it doesnt exists */
        registerMobileNumber:
        function (mobileNo,callback) {
            
           connection.query(
                ` INSERT IGNORE INTO registration(mobile) VALUES(?)`,
            [mobileNo],
            function (err, rows, fields) {
            
                if (err) throw err;
                callback(rows);
    
            });
            
    
        },
        
            /* check if contact exists */
            checkContacts:
            function (mobileNo,callback) {
                
               connection.query(
                    `SELECT DISTINCT mobile 
                     FROM registration
                     WHERE mobile IN (?)`,
                [mobileNo],
                function (err, rows, fields) {
                
                    if (err) throw err;
                    callback(rows);
        
                });
                
        
            }
        }    



       