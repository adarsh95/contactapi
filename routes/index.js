var express = require('express');
var router = express.Router();
var Contact = require("../model/contact");
var bodyParser = require('body-parser');

router.get('/', function (req, res, next) {

  res.render('index', { title: 'Express' });
});

/* GET home page. */
router.post('/sendContacts', function (req, res, next) {
  var contacts = [];
  var testData = JSON.parse(req.body.data);
  var mobile_no = req.body.mobile_number;
  var existsContact = [];
  var final = {};
  var matchData = [];
  var unMatchData = [];

  //insert into database if its not  registered
  Contact.registerMobileNumber(mobile_no, function (result) {
  });




  //push all mobile nos into an array 
  for (i = 0; i < testData.length; i++) {
    for (j = 0; j < testData[i].phone.length; j++) {
      contacts.push(testData[i].phone[j]);
    }
  }
  //check if contacts exists
  Contact.checkContacts(contacts, function (resultContactsExists) {
    //if any contact exists
    for (i = 0; i < resultContactsExists.length; i++) {
      existsContact[i] = (resultContactsExists[i].phone);
    }
    //push final array for response
    for (i = 0; i < testData.length; i++) {
      var temp = 0;
      for (j = 0; j < testData[i].phone.length; j++) {
        for (k = 0; k < existsContact.length; k++) {
          if (testData[i].phone[j] == existsContact[k] && temp == 0) {

            matchData.push(testData[i]);
            temp++;
          }
        }
      }
      if (temp == 0) {
        unMatchData.push(testData[i]);

      }

    }

    final.match = matchData;
    final.unmatch = unMatchData;
    response = {
      'status': '1',
      'msg': 'Success',
      'data': final
    };

    res.json(response);

  });
});

module.exports = router;
